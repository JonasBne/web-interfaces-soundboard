import { useCallback, useEffect, useState } from "react";
import axios from "axios";

const useGetCollection = (url: string) => {
  const [fetchedData, setFetchedData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const refetch = async () => getData();

  const getData = useCallback(async () => {
    setIsLoading(true);
    try {
      const response = await axios.get(url);

      setFetchedData(response.data);

      setIsLoading(false);
    } catch {
      alert("Error occured");
    }
    setIsLoading(false);
  }, [url]);

  useEffect(() => {
    getData();
  }, [getData]);

  return {
    isLoading,
    data: fetchedData,
    refetch,
  };
};

export default useGetCollection;
