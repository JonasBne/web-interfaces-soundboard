import React from "react";

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import Soundboard from "./components/Soundboard";
import Soundboards from "./components/Soundboards";

function App() {
  return (
    <Router>
      <div className="App">
        <Switch>
          <Route path="/soundboards/:id">
            <Soundboard />
          </Route>
          <Route path="/">
            <Soundboards />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
