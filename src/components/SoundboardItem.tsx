import React, { useState } from "react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPauseCircle, faPlayCircle } from "@fortawesome/free-solid-svg-icons";

import styles from "./SoundboardItem.module.css";


interface Item {
  name: string;
  image: string;
  id: number;
  sound: string;
  quote: string;
  soundboardId: number;
}

interface IProps {
  item: Item;
  onPlaySound: (quote: string) => void;
  onSoundStop: () => void;
}

const SoundboardItem = ({ item, onPlaySound, onSoundStop }: IProps) => {
  const [isPlaying, setIsPlaying] = useState(false);
  const [showPlayButton, setShowPlayButton] = useState(true);
  const [showPauseButton, setShowPauseButton] = useState(false);
  const [audio] = useState(new Audio(item.sound));

  audio.addEventListener("ended", () => {
    setIsPlaying(false);
  });

  const playSound = () => {
    setShowPlayButton(false);
    setShowPauseButton(true);

    if (!isPlaying) {
      audio.play();
      setIsPlaying(true);
      onPlaySound(item.quote);
    }
  };

  const pauseSound = () => {
    setShowPlayButton(true);
    setShowPauseButton(false);

    if (isPlaying) {
      audio.pause();
      setIsPlaying(false);
      onSoundStop();
    }
  };

  return (
    <div>
      {showPlayButton && (
        <FontAwesomeIcon
          icon={faPlayCircle}
          className={styles["icon--play"]}
          onClick={playSound}
        />
      )}
      {showPauseButton && (
          <FontAwesomeIcon
            icon={faPauseCircle}
            className={styles["icon--pause"]}
            onClick={pauseSound}
          />
      )}
      <div className={styles.container}>
        <img
          src={item.image}
          alt={item.name}
          className={`${isPlaying ? styles["image--active"] : styles.image}`}
        ></img>
      </div>
    </div>
  );
};

export default SoundboardItem;

