import React, { useState, useReducer, useEffect } from "react";
import axios from "axios";
import { useParams } from "react-router";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlusCircle } from "@fortawesome/free-solid-svg-icons";
import { faTimesCircle } from "@fortawesome/free-solid-svg-icons";

import styles from "./Soundboard.module.css";
import SoundboardItem from "./SoundboardItem";
import Spinner from "./Spinner";
import useGetCollection from "../useGetCollection";
import Form from "./Form";

axios.defaults.baseURL = "http://localhost:3001";

interface ISoundboardItem {
  name: string;
  image: string;
  sound: string;
  id: number;
  quote: string;
  soundboardId: number;
}


interface IAction {
  type: string;
  val?: any;
}

const initialFormState = {
  title: "",
  quote: "",
  image: "",
  sound: "",
  titleIsValid: undefined,
  quoteIsValid: undefined,
  imageIsValid: undefined,
  soundIsValid: undefined,
  formIsValid: false,
  soundboardId: undefined,
};

const formReducer = (state: any, action: IAction) => {
  if (action.type === "INPUT_TITLE") {
    return {
      ...state,
      title: action.val,
      titleIsValid: action.val.trim().length > 4 ? true : false,
    };
  } else if (action.type === "INPUT_QUOTE") {
    return {
      ...state,
      quote: action.val,
      quoteIsValid: action.val.trim().length > 4 ? true : false,
    };
  } else if (action.type === "INPUT_IMAGE") {
    return {
      ...state,
      image: action.val,
      imageIsValid: action.val.trim().length > 4 ? true : false,
    };
  } else if (action.type === "INPUT_SOUND") {
    return {
      ...state,
      sound: action.val,
      soundIsValid: action.val.trim().length > 4 ? true : false,
    };
  } else {
    return initialFormState;
  }
};

const Soundboard = () => {
  const [quote, setQuote] = useState(null);
  const [showForm, setShowForm] = useState(false);
  const [formState, dispatchForm] = useReducer(formReducer, initialFormState);
  const { id } = useParams<{ id: string }>()

  useEffect(() => {
    if (
      formState.titleIsValid &&
      formState.quoteIsValid &&
      formState.imageIsValid &&
      formState.soundIsValid
    ) {
      formState.formIsValid = true;
    }
  }, [
    formState,
    formState.titleIsValid,
    formState.quoteIsValid,
    formState.imageIsValid,
    formState.soundIsValid,
    formState.formIsValid,
  ]);

  const {
    isLoading,
    data: soundboardItems,
    refetch,
  } = useGetCollection(
    `http://localhost:3001/soundboards/${id}/items`
  );

  const addItem = (name: string, quote: string, image: string, sound: string) => {
    // https://stackoverflow.com/questions/60368017/await-has-no-effect-on-the-type-of-this-expression/62487550
    return axios.post("/items", {
      name: name,
      quote: quote,
      image: image,
      sound: sound,
      soundboardId: id,
    }); // return a promise
  };

  const addButtonClickHandler = () => {
    setShowForm(true);
  };

  const cancelButtonClickHandler = () => {
    setShowForm(false);
  };

  const addTrackToSoundboardHandler = async () => {
    const resp = await addItem(formState.title, formState.quote, formState.image, formState.sound); // wait until promise is fulfilled

    if (resp.status === 201) {
      refetch();
    }

    setShowForm(false);
  };

  return (
    <React.Fragment>
      {quote && <div className={styles.quote}>{quote}</div>}
      {isLoading ? (
        <Spinner message={"Loading soundboard items..."} />
      ) : (
        <div>
          <div className={styles["container--title"]}>
            <div className={styles["container--add"]}>
              {showForm ? (
                <span className={styles.text}>Cancel</span>
              ) : (
                <span className={styles.text}>Add new track</span>
              )}
              {showForm ? (
                <FontAwesomeIcon
                  icon={faTimesCircle}
                  className={styles["icon--cancel"]}
                  onClick={cancelButtonClickHandler}
                />
              ) : (
                <FontAwesomeIcon
                  icon={faPlusCircle}
                  className={styles["icon--plus"]}
                  onClick={addButtonClickHandler}
                />
              )}
            </div>
          </div>
          {showForm ? (
            <Form formState={formState} dispatchForm={dispatchForm} onFormSubmit={addTrackToSoundboardHandler} />
          ) : null}
          <div className={styles.soundboard}>
            {soundboardItems.map((item: ISoundboardItem) => {
              return (
                <SoundboardItem
                  key={item.id}
                  item={item}
                  onPlaySound={(quote: any) => setQuote(quote)}
                  onSoundStop={() => setQuote(null)}
                />
              );
            })}
          </div>
        </div>
      )}
    </React.Fragment>
  );
};

export default Soundboard;
