import React from "react";

import styles from "./Spinner.module.css"; 

interface ISpinner {
  message: string;
}

const Spinner = ({ message }: ISpinner) => {
  return (
    <div className={styles["loading-box"]}>
      <div className= {styles["loading-icon"]}></div>
      <div className= {styles.loading}>{message}</div>
    </div>
  );
};

export default Spinner;
