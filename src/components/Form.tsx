import React, { ChangeEvent } from "react";

import styles from "./Form.module.css";

interface IDispatch {
  type: string;
  val: any;
}

interface IFormState {
  title: string;
  quote: string;
  image: string;
  sound: string;
  titleIsValid?: boolean;
  quoteIsValid?: boolean;
  imageIsValid?: boolean;
  soundIsValid?: boolean;
  formIsValid: boolean;
  soundboardId: 1;
}

interface IForm {
  formState: IFormState;
  onFormSubmit: () => void;
  dispatchForm: ({ type, val }: IDispatch) => void;
}

const Form = ({ formState, dispatchForm, onFormSubmit }: IForm) => {
  const inputChangeHandler = (ev: ChangeEvent<HTMLInputElement>) => {
    if (ev.target.name === "title") {
      dispatchForm({ type: "INPUT_TITLE", val: ev.target.value });
    } else if (ev.target.name === "quote") {
      dispatchForm({ type: "INPUT_QUOTE", val: ev.target.value });
    } else if (ev.target.name === "image") {
      dispatchForm({ type: "INPUT_IMAGE", val: ev.target.value });
    } else if (ev.target.name === "sound") {
      dispatchForm({ type: "INPUT_SOUND", val: ev.target.value });
    }
  };

  const formSubmitHandler = (ev: React.MouseEvent) => {
    ev.preventDefault();

    if (formState.formIsValid) {
      onFormSubmit();
    } else {
      alert("Please fill in all fields before you submit!");
    }
  };

  return (
    <form className={styles.form}>
      <h1>Add new track</h1>

      <input
        name="title"
        type="text"
        placeholder="Track title..."
        onChange={inputChangeHandler}
        className={`${
          formState.titleIsValid === false ? styles["input--invalid"] : ""
        }`}
        value={formState.title}
      />
      <input
        name="quote"
        type="text"
        placeholder="Quote..."
        onChange={inputChangeHandler}
        className={`${
          formState.quoteIsValid === false ? styles["input--invalid"] : ""
        }`}
        value={formState.quote}
      />
      <input
        name="image"
        type="text"
        placeholder="URL to image..."
        onChange={inputChangeHandler}
        className={`${
          formState.imageIsValid === false ? styles["input--invalid"] : ""
        }`}
        value={formState.image}
      />
      <input
        name="sound"
        type="text"
        placeholder="URL to sound fragment..."
        onChange={inputChangeHandler}
        className={`${
          formState.soundIsValid === false ? styles["input--invalid"] : ""
        }`}
        value={formState.sound}
      />
      <button
        type={"submit"}
        className={styles["button-action"]}
        onClick={formSubmitHandler}
      >
        Add
      </button>
    </form>
  );
};

export default Form;
