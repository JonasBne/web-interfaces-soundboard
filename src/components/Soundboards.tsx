import React from "react";
import { useHistory } from "react-router-dom";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import { CardActionArea } from "@mui/material";

import styles from "./Soundboards.module.css";

const Soundboards = () => {
  const history = useHistory();

  return (
    <React.Fragment>
      <h1 className={styles.title}>Soundboard</h1>
      <div className={styles["card-container"]}>
        <Card sx={{ maxWidth: 345 }} onClick={() => history.push("/soundboards/1/items")}>
          <CardActionArea >
            <CardMedia
              component="img"
              height="140"
              image="https://www.bensound.com/bensound-img/memories.jpg"
              alt="Bensound soundboard"
            />
            <CardContent>
              <Typography gutterBottom variant="h5" component="div">
                BENSOUND
              </Typography>
              <Typography variant="body2" color="text.secondary">
                Enjoy some royalty free music from Bensound!
              </Typography>
            </CardContent>
          </CardActionArea>
        </Card>

        <Card sx={{ maxWidth: 345 }} onClick={() => history.push("/soundboards/2/items")}>
          <CardActionArea>
            <CardMedia
              component="img"
              height="140"
              image="https://www.bensound.com/bensound-img/tomorrow.jpg"
              alt="Soundzone soundboard"
            />
            <CardContent>
              <Typography gutterBottom variant="h5" component="div">
                SOUNDZONE
              </Typography>
              <Typography variant="body2" color="text.secondary">
                Enjoy some royalty free music from Soundzone!
              </Typography>
            </CardContent>
          </CardActionArea>
        </Card>
      </div>
    </React.Fragment>
  );
};

export default Soundboards;
